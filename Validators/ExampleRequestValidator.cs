using FluentValidation;
using ParametersExampleApi.Representations;

namespace ParametersExampleApi.Validators;

public class ExampleRequestValidator : AbstractValidator<ExampleRequest>
{
    public ExampleRequestValidator()
    {
        RuleFor(x => x.Id).GreaterThan(0);

        RuleFor(x => x.Value).MinimumLength(3);

        RuleFor(x => x.Body.ValueInBody1).MinimumLength(5).WithName(nameof(ExampleRequestBody.ValueInBody1));

        RuleFor(x => x.Body.ValueInBody2).NotEmpty().WithName(nameof(ExampleRequestBody.ValueInBody2));;
    }
}