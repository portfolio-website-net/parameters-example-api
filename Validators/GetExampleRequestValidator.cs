using FluentValidation;
using ParametersExampleApi.Representations;

namespace ParametersExampleApi.Validators;

public class GetExampleRequestValidator : AbstractValidator<GetExampleRequest>
{
    public GetExampleRequestValidator()
    {
        RuleFor(x => x.Id).GreaterThan(0);

        RuleFor(x => x.Value).MinimumLength(3);
    }
}