using Microsoft.AspNetCore.Mvc;
 
namespace ParametersExampleApi.Controllers;
 
[ApiController]
[Route("api/[controller]/{Id}")]
public class Example9Controller : ControllerBase
{
    [HttpPost("example9/{Value}")]
    public Example9Response PostExample9(Example9Request request)
    {
        return new Example9Response
        {
            Id = request.Id,
            Value = request.Value,
            ValueInBody1 = request.Body.ValueInBody1,
            ValueInBody2 = request.Body.ValueInBody2
        };
    }
 
    public class Example9Request
    {
        [FromRoute]
        public int Id { get; set; }

        [FromRoute]
        public string Value { get; set; }

        [FromBody]
        public Example9RequestBody Body { get; set; }
    }
 
    public class Example9RequestBody
    {
        public string ValueInBody1 { get; set; }

        public string ValueInBody2 { get; set; }
    }
     
    public class Example9Response
    {
        public int Id { get; set; }

        public string Value { get; set; }

        public string ValueInBody1 { get; set; }
        
        public string ValueInBody2 { get; set; }
    }
}