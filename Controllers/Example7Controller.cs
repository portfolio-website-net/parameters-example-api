using Microsoft.AspNetCore.Mvc;
 
namespace ParametersExampleApi.Controllers;
 
[ApiController]
[Route("api/[controller]/{Id}")]
public class Example7Controller : ControllerBase
{
    [HttpPost("example7/{Value}")]
    public Example7Response PostExample7(Example7Request request)
    {
        return new Example7Response
        {
            Id = request.Id,
            Value = request.Value,
            ValueInBody = request.ValueInBody
        };
    }
 
    public class Example7Request
    {
        [FromRoute]
        public int Id { get; set; }

        [FromRoute]
        public string Value { get; set; }

        [FromBody]
        public string ValueInBody { get; set; }
    }
     
    public class Example7Response
    {
        public int Id { get; set; }
        
        public string Value { get; set; }

        public string ValueInBody { get; set; }
    }
}