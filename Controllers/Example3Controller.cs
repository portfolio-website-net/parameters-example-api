using Microsoft.AspNetCore.Mvc;
 
namespace ParametersExampleApi.Controllers;
 
[ApiController]
[Route("api/[controller]/{Id}")]
public class Example3Controller : ControllerBase
{
    [HttpGet("example3/{Value}")]
    public Example3Response GetExample3(Example3Request request)
    {
        return new Example3Response
        {
            Id = request.Id,
            Value = request.Value
        };
    }
 
    public class Example3Request
    {
        public int Id { get; set; }

        public string Value { get; set; }
    }
 
    public class Example3Response
    {
        public int Id { get; set; }
        
        public string Value { get; set; }
    }
}