using Microsoft.AspNetCore.Mvc;
 
namespace ParametersExampleApi.Controllers;
 
[ApiController]
[Route("api/[controller]/{Id}")]
public class Example8Controller : ControllerBase
{
    [HttpPost("example8/{Value}")]
    public Example8Response PostExample8(Example8Request request)
    {
        return new Example8Response
        {
            Id = request.Id,
            Value = request.Value,
            ValueInBody1 = request.ValueInBody1,
            ValueInBody2 = request.ValueInBody2
        };
    }
 
    public class Example8Request
    {
        [FromRoute]
        public int Id { get; set; }

        [FromRoute]
        public string Value { get; set; }

        [FromBody]
        public string ValueInBody1 { get; set; }

        [FromBody]
        public string ValueInBody2 { get; set; }
    }
     
    public class Example8Response
    {
        public int Id { get; set; }

        public string Value { get; set; }

        public string ValueInBody1 { get; set; }
        
        public string ValueInBody2 { get; set; }
    }
}