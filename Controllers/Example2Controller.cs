using Microsoft.AspNetCore.Mvc;
 
namespace ParametersExampleApi.Controllers;
 
[ApiController]
[Route("api/[controller]/{id}")]
public class Example2Controller : ControllerBase
{
    [HttpGet("example2/{value}")]
    public Example2Response GetExample2(Example2Request request)
    {
        return new Example2Response
        {
            Id = request.Id,
            Value = request.Value
        };
    }
 
    public class Example2Request
    {
        public int Id { get; set; }
        
        public string Value { get; set; }
    }
 
    public class Example2Response
    {
        public int Id { get; set; }

        public string Value { get; set; }
    }
}