using Microsoft.AspNetCore.Mvc;
using ParametersExampleApi.Representations;
using ParametersExampleApi.Validators;

namespace ParametersExampleApi.Controllers;
 
[ApiController]
[Route("api/[controller]/{Id}")]
public class SolutionsController : ControllerBase
{
    [HttpGet("solutions/{Value}")]
    public GetExampleResponse GetExample(GetExampleRequest request)
    {
        return new GetExampleResponse
        {
            Id = request.Id,
            Value = request.Value
        };
    }

    [HttpPost("solutions/{Value}")]
    public ExampleResponse PostExample(ExampleRequest request)
    {
        return GetResponse(request);
    }

    [HttpPut("solutions/{Value}")]
    public ExampleResponse PutExample(ExampleRequest request)
    {
        return GetResponse(request);
    }

    [HttpPatch("solutions/{Value}")]
    public ExampleResponse PatchExample(ExampleRequest request)
    {
        return GetResponse(request);
    }

    [HttpDelete("solutions/{Value}")]
    public ExampleResponse DeleteExample(ExampleRequest request)
    {
        return GetResponse(request);
    }

    [HttpOptions("solutions/{Value}")]
    public ExampleResponse OptionsExample(ExampleRequest request)
    {
        return GetResponse(request);
    }

    private ExampleResponse GetResponse(ExampleRequest request)
    {
        return new ExampleResponse
        {
            Id = request.Id,
            Value = request.Value,
            ValueInBody1 = request.Body.ValueInBody1,
            ValueInBody2 = request.Body.ValueInBody2
        };
    }
}