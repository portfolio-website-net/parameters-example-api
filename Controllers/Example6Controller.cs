using Microsoft.AspNetCore.Mvc;
 
namespace ParametersExampleApi.Controllers;
 
[ApiController]
[Route("api/[controller]/{Id}")]
public class Example6Controller : ControllerBase
{
    [HttpPost("example6/{Value}")]
    public Example6Response PostExample6(Example6Request request)
    {
        return new Example6Response
        {
            Id = request.Id,
            Value = request.Value,
            ValueInQueryString = request.ValueInQueryString
        };
    }
 
    public class Example6Request
    {
        public int Id { get; set; }

        public string Value { get; set; }

        public string ValueInQueryString { get; set; }
    }
     
    public class Example6Response
    {
        public int Id { get; set; }

        public string Value { get; set; }
        
        public string ValueInQueryString { get; set; }
    }
}