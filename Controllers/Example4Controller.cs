using Microsoft.AspNetCore.Mvc;
 
namespace ParametersExampleApi.Controllers;
 
[ApiController]
[Route("api/[controller]/{id}")]
public class Example4Controller : ControllerBase
{
    [HttpPost("example4/{value}")]
    public Example4Response PostExample4(int id, string value)
    {
        return new Example4Response
        {
            Id = id,
            Value = value
        };
    }
 
    public class Example4Response
    {
        public int Id { get; set; }
        
        public string Value { get; set; }
    }
}