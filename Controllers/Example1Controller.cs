using Microsoft.AspNetCore.Mvc;
 
namespace ParametersExampleApi.Controllers;
 
[ApiController]
[Route("api/[controller]/{id}")]
public class Example1Controller : ControllerBase
{
    [HttpGet("example1/{value}")]
    public Example1Response GetExample1(int id, string value)
    {
        return new Example1Response
        {
            Id = id,
            Value = value
        };
    }
 
    public class Example1Response
    {
        public int Id { get; set; }
        
        public string Value { get; set; }
    }
}