using Microsoft.AspNetCore.Mvc;
 
namespace ParametersExampleApi.Controllers;
 
[ApiController]
[Route("api/[controller]/{Id}")]
public class Example5Controller : ControllerBase
{
    [HttpPost("example5/{Value}")]
    public Example5Response PostExample5(Example5Request request)
    {
        return new Example5Response
        {
            Id = request.Id,
            Value = request.Value
        };
    }
 
    public class Example5Request
    {
        public int Id { get; set; }

        public string Value { get; set; }
    }
     
    public class Example5Response
    {
        public int Id { get; set; }
        
        public string Value { get; set; }
    }
}