namespace ParametersExampleApi.Representations;

public class ExampleRequest : BaseExampleRequest<ExampleRequestBody>
{
}

public class ExampleRequestBody
{
    public string ValueInBody1 { get; set; }
    
    public string ValueInBody2 { get; set; }
}