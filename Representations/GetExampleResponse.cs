namespace ParametersExampleApi.Representations;

public class GetExampleResponse
{
    public int Id { get; set; }
    
    public string Value { get; set; }
}