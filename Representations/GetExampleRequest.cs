namespace ParametersExampleApi.Representations;

public class GetExampleRequest
{
    public int Id { get; set; }

    public string Value { get; set; }
}