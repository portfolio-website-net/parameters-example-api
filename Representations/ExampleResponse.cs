namespace ParametersExampleApi.Representations;

public class ExampleResponse
{
    public int Id { get; set; }

    public string Value { get; set; }

    public string ValueInBody1 { get; set; }

    public string ValueInBody2 { get; set; }
}