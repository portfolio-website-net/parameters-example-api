using Microsoft.AspNetCore.Mvc;

namespace ParametersExampleApi.Representations;

public abstract class BaseExampleRequest<T>
{
    [FromRoute]
    public int Id { get; set; }

    [FromRoute]
    public string Value { get; set; }

    [FromBody]
    public T Body { get; set; }
}